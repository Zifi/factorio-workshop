/// <reference types="Cypress" />


//todo predelat na globalni
Cypress.Commands.add('resetGame', (options = {}) =>{
    cy.request('POST', "http://localhost:8080/restart")
});

Cypress.Commands.add('addTurn', (playerName, action, x, y) =>{
    cy.request('POST', 'http://localhost:8080/turns', {PlayerName: playerName, Action: action, Coordinates: {X: x, Y: y}})
});

Cypress.Commands.add('getBank', (options = {}) =>{
    cy.request('GET', "http://localhost:8080/bank").then(response => {
        const target = (response.statusText)
        return target
    })
    
});

Cypress.Commands.add('getBankV2', (options = {}) =>{
    cy.request('GET', "http://localhost:8080/bank").then(response => {
        const target = (response.allRequestResponses)
        return target
    })
    
});

Cypress.Commands.add('getWorld', (options = {}) =>{
    return cy.request({
        url: 'http://localhost:8080/bank'     
    }).then(response => {
        const target = (response.body)
        return target
    })


    
});

