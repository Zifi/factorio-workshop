 ///<reference types="Cypress" />

Cypress.Commands.add('startGame', (playerName, options = {}) => {
    cy.visit('/');
    cy.get('select').select(playerName);
    cy.get('button').click(); // play as default player
});

Cypress.Commands.add('tickGame', (playerName, options = {}) => {
    cy.wait(1000); // game ticks like that
});

Cypress.Commands.add('delay', (miliseconds, options = {}) => {
    cy.wait(miliseconds); // game ticks like that
});


//find first
Cypress.Commands.add('findFirstStone', (playerName, options = {}) => {
    return cy.get('td > div > img[alt="Stone Icon"]:first');
});

Cypress.Commands.add('findFirstOil', (playerName, options = {}) => {
    return cy.get('td > div > img[alt="Oil Icon"]:first');
});

Cypress.Commands.add('findFirstCopper', (playerName, options = {}) => {
    return cy.get('td > div > img[alt="CopperOre Icon"]:first');
});

Cypress.Commands.add('findFirstEmptyField', (playerName, options = {}) => {
    return cy.get('td > div > img[alt="None Icon"]:first');
});

Cypress.Commands.add('findFirstIron', (playerName, options = {}) => {
    return cy.get('td > div > img[alt="IronOre Icon"]:first');
});

//mine and build
Cypress.Commands.add('mineResource', (playerName, options = {}) => {
    cy.get('#action-MineResource').click();
});

Cypress.Commands.add('buildMine', (playerName, options = {}) => {
    cy.get('#action-BuildMine').click();
});


//deposits stone and coal
Cypress.Commands.add('stoneDeposit', (options = {}) => {
    return cy.get('#bank-Stone-amount').invoke('text');
});


//deposits oil
Cypress.Commands.add('oilDeposit', (options = {}) => {
    return cy.get('#bank-Oil-amount').invoke('text');
});

Cypress.Commands.add('petroleumDeposit', (options = {}) => {
    return cy.get('#bank-Petroleum-amount').invoke('text');
});

Cypress.Commands.add('basicFuelDeposit', (options = {}) => {
    return cy.get('#bank-BasicFuel-amount').invoke('text');
});

Cypress.Commands.add('solidFuelDeposit', (options = {}) => {
    return cy.get('#bank-SolidFuel-amount').invoke('text');
});

Cypress.Commands.add('rocketFuelDeposit', (options = {}) => {
    return cy.get('#bank-RocketFuel-amount').invoke('text');
});

//deposits copper
Cypress.Commands.add('copperOreDeposit', (options = {}) => {
    return cy.get('#bank-CopperOre-amount').invoke('text');
});

Cypress.Commands.add('copperPlateDeposit', (options = {}) => {
    return cy.get('#bank-CopperPlate-amount').invoke('text');
});

Cypress.Commands.add('wireDeposit', (options = {}) => {
    return cy.get('#bank-Wire-amount').invoke('text');
});

Cypress.Commands.add('circuitDeposit', (options = {}) => {
    return cy.get('#bank-Circuit-amount').invoke('text');
});

Cypress.Commands.add('computerDeposit', (options = {}) => {
    return cy.get('#bank-Computer-amount').invoke('text');
});

//deposit steel iron
Cypress.Commands.add('ironOreDeposit', (options = {}) => {
    return cy.get('#bank-IronOre-amount').invoke('text');
});

Cypress.Commands.add('ironPlateDeposit', (options = {}) => {
    return cy.get('#bank-IronPlate-amount').invoke('text');
});

Cypress.Commands.add('steelDeposit', (options = {}) => {
    return cy.get('#bank-Steel-amount').invoke('text');
});

Cypress.Commands.add('steelCasingDeposit', (options = {}) => {
    return cy.get('#bank-SteelCasing-amount').invoke('text');
});

Cypress.Commands.add('rocketShellDeposit', (options = {}) => {
    return cy.get('#bank-RocketShell-amount').invoke('text');
});


//build fuel 
Cypress.Commands.add('buildPetroleumFactory', (playerName, options = {}) => {
    cy.get('#action-BuildPetroleumFactory').click();
});

Cypress.Commands.add('buildBasicFuelFactory', (playerName, options = {}) => {
    cy.get('#action-BuildBasicFuelFactory').click();
});

Cypress.Commands.add('buildSolidFuelFactory', (playerName, options = {}) => {
    cy.get('#action-BuildSolidFuelFactory').click();
});

Cypress.Commands.add('buildRocketFuelFactory', (playerName, options = {}) => {
    cy.get('#action-BuildRocketFuelFactory').click();
});


//build copper
Cypress.Commands.add('buildCopperPlateFactory', (playerName, options = {}) => {
    cy.get('#action-BuildCopperPlateFactory').click();
});

Cypress.Commands.add('buildWireFactory', (playerName, options = {}) => {
    cy.get('#action-BuildWireFactory').click();
});

Cypress.Commands.add('buildCircuitFactory', (playerName, options = {}) => {
    cy.get('#action-BuildCircuitFactory').click();
});

Cypress.Commands.add('buildComputerFactory', (playerName, options = {}) => {
    cy.get('#action-BuildComputerFactory').click();
});

//build iron steel
Cypress.Commands.add('buildSteelFactory', (playerName, options = {}) => {
    cy.get('#action-BuildSteelFactory').click();
});

Cypress.Commands.add('buildIronPlateFactory', (playerName, options = {}) => {
    cy.get('#action-BuildIronPlateFactory').click();
});

Cypress.Commands.add('buildSteelCasingFactory', (playerName, options = {}) => {
    cy.get('#action-BuildSteelCasingFactory').click();
});

Cypress.Commands.add('buildRocketShellFactory', (playerName, options = {}) => {
    cy.get('#action-BuildRocketShellFactory').click();
});
