///<reference types="Cypress" />


//mining
Cypress.Commands.add('stoneMining', (playerName, options = {}) => {

    for (let i = 0; i < 6; i++) {
        cy.findFirstStone().click();
        cy.mineResource();
    }

    cy.findFirstStone().click();
    cy.buildMine();

    cy.delay(2000);

    for (let i = 0; i < 4; i++) {
        cy.reloadPageAndStartGame(playerName);

        cy.findFirstStone().click();
        cy.buildMine();
    }
});

Cypress.Commands.add('oilMining', (playerName, oilMineCount,  options = {}) => {

    for (let i = 0; i < oilMineCount; i++) {
        cy.reloadPageAndStartGame(playerName);
        
        
        cy.findFirstOil().click();
        cy.buildMine();

        
    }
});

Cypress.Commands.add('copperMining', (playerName, oilMineCount,  options = {}) => {

    for (let i = 0; i < oilMineCount; i++) {
        cy.reloadPageAndStartGame(playerName);
        
        
        cy.findFirstCopper().click();
        cy.buildMine();

        
    }
});

Cypress.Commands.add('ironMining', (playerName, ironMineCount,  options = {}) => {

    for (let i = 0; i < ironMineCount; i++) {
        cy.reloadPageAndStartGame(playerName);
        
        
        cy.findFirstIron().click();
        cy.buildMine();

        
    }
});


//building fuel
Cypress.Commands.add('buildPetroleumFactoryBuild', (playerName, buildCount, options = {}) => {

    for (let i = 0; i < buildCount; i++) {
        cy.reloadPageAndStartGame(playerName);

        cy.findFirstEmptyField().click();
        cy.buildPetroleumFactory();
    }
});

Cypress.Commands.add('buildBasicFuelFactoryBuild', (playerName, buildCount, options = {}) => {

    for (let i = 0; i < buildCount; i++) {
        cy.reloadPageAndStartGame(playerName);

        cy.findFirstEmptyField().click();
        cy.buildBasicFuelFactory();

    }
});

Cypress.Commands.add('buildSolidFuelFactoryBuild', (playerName, buildCount, options = {}) => {

    for (let i = 0; i < buildCount; i++) {
        cy.reloadPageAndStartGame(playerName);

        cy.findFirstEmptyField().click();
        cy.buildSolidFuelFactory();

    }
});

Cypress.Commands.add('buildRocketFuelFactoryBuild', (playerName, buildCount, options = {}) => {

    for (let i = 0; i < buildCount; i++) {
        cy.reloadPageAndStartGame(playerName);
        
        cy.findFirstEmptyField().click();
        cy.buildRocketFuelFactory();
    }
});

//building copper 
Cypress.Commands.add('buildCopperPlateFactoryBuild', (playerName, buildCount, options = {}) => {

    for (let i = 0; i < buildCount; i++) {
        cy.reloadPageAndStartGame(playerName);

        cy.findFirstEmptyField().click();
        cy.buildCopperPlateFactory();
    }
});

Cypress.Commands.add('buildWireFactoryBuild', (playerName, buildCount, options = {}) => {

    for (let i = 0; i < buildCount; i++) {
        cy.reloadPageAndStartGame(playerName);

        cy.findFirstEmptyField().click();
        cy.buildWireFactory();

    }
});

Cypress.Commands.add('buildCircuitFactoryBuild', (playerName, buildCount, options = {}) => {

    for (let i = 0; i < buildCount; i++) {
        cy.reloadPageAndStartGame(playerName);

        cy.findFirstEmptyField().click();
        cy.buildCircuitFactory();

    }
});

Cypress.Commands.add('buildComputerFactoryBuild', (playerName, buildCount, options = {}) => {

    for (let i = 0; i < buildCount; i++) {
        cy.reloadPageAndStartGame(playerName);
        
        cy.findFirstEmptyField().click();
        cy.buildComputerFactory();
    }
});

//building steel iron
Cypress.Commands.add('buildIronPlateFactoryBuild', (playerName, buildCount, options = {}) => {

    for (let i = 0; i < buildCount; i++) {
        cy.reloadPageAndStartGame(playerName);

        cy.findFirstEmptyField().click();
        cy.buildIronPlateFactory();
    }
});

Cypress.Commands.add('buildSteelFactoryBuild', (playerName, buildCount, options = {}) => {

    for (let i = 0; i < buildCount; i++) {
        cy.reloadPageAndStartGame(playerName);

        cy.findFirstEmptyField().click();
        cy.buildSteelFactory();

    }
});

Cypress.Commands.add('buildSteelCasingFactoryBuild', (playerName, buildCount, options = {}) => {

    for (let i = 0; i < buildCount; i++) {
        cy.reloadPageAndStartGame(playerName);

        cy.findFirstEmptyField().click();
        cy.buildSteelCasingFactory();

    }
});

Cypress.Commands.add('buildRocketShellFactoryBuild', (playerName, buildCount, options = {}) => {

    for (let i = 0; i < buildCount; i++) {
        cy.reloadPageAndStartGame(playerName);
        
        cy.findFirstEmptyField().click();
        cy.buildRocketShellFactory();
    }
});

//help commands 
Cypress.Commands.add('reloadPageAndStartGame', (playerName, options = {}) => { 

        cy.reload();
        cy.startGame(playerName);
        cy.delay(2000);
});