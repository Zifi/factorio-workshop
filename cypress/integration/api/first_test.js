context('Mining by hand', () => {
    beforeEach(() => {
        cy.startGame('Berlin');
    });

    it('Init Stone Production2!', function () {
        for (let i = 0; i < 5; i++) {
             cy.addTurn("Professor", "MineResource",10,0);
            cy.addTurn("Nairobi", "MineResource",1,1);
            cy.addTurn("Tokyo", "MineResource",2,2);
            cy.addTurn("Berlin", "MineResource",3,3);
            cy.addTurn("Denver", "MineResource",4,4);
            cy.addTurn("Helsinki", "MineResource",5,5);
            cy.addTurn("Rio", "MineResource",6,6);
            cy.addTurn("Moscow", "MineResource",7,7);
            cy.addTurn("Oslo", "MineResource",8,8);
        }     
    })
});

