
context('Iron / steel test complete', () => {
    beforeEach(() => {
      cy.resetGame();
       cy.startGame('Professor');
    });
 
    it('Iron!', function () {
 
       // basic materials
       cy.stoneMining('Professor',6);
       cy.stoneDeposit().should('be.above', 1);
 
       cy.ironMining('Professor' ,6);
       cy.ironOreDeposit().should('be.above', 1);
  
       //factoryes
       cy.buildIronPlateFactoryBuild('Professor',6);
       cy.ironPlateDeposit().should('be.above', 1);
 
       cy.buildSteelFactoryBuild('Professor',6);
       cy.steelDeposit().should('be.above', 1);
 
       cy.buildSteelCasingFactoryBuild('Professor',6);
       cy.steelCasingDeposit().should('be.above', 1);
 
       cy.buildRocketShellFactoryBuild('Professor', 6);
       cy.rocketShellDeposit().should('be.above', 1);
    })
 });