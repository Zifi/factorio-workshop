
context('Rocket fuel test complete', () => {
    beforeEach(() => {
      cy.resetGame();
       cy.startGame('Professor');
    });
 
    it('Rocket!', function () {
 
      // basic materials
       cy.stoneMining('Professor',6);
       cy.stoneDeposit().should('be.above', 1);
 
       cy.copperMining('Professor' ,6);
       cy.copperOreDeposit().should('be.above', 1);
  
       //factoryes
       cy.buildCopperPlateFactoryBuild('Professor',6);
       cy.copperPlateDeposit().should('be.above', 1);
 
       cy.buildWireFactoryBuild('Professor',6);
       cy.wireDeposit().should('be.above', 1);
 
       cy.buildCircuitFactoryBuild('Professor',6);
       cy.circuitDeposit().should('be.above', 1);
 
       cy.buildComputerFactoryBuild('Professor', 6);
       cy.computerDeposit().should('be.above', 1);
    })
 });
