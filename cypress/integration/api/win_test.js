context('Mining by hand', () => {
    beforeEach(() => {
        cy.startGame('Tokyo');
    });

    it('Init Stone Production2!', function () {
        //cy.addTurn("Tokyo", "MineResource", 0, 10);
       
        for (let x = 0; x < 29; x++) {
            for(let y = 0; y < 49; y++) 
            cy.addTurn("Tokyo", "MineResource",x,y);
            // cy.addTurn("Nairobi", "MineResource",1,1);
            // cy.addTurn("Tokyo", "MineResource",2,2);
            // cy.addTurn("Berlin", "MineResource",3,3);
            // cy.addTurn("Denver", "MineResource",4,4);
            // cy.addTurn("Helsinki", "MineResource",5,5);
            // cy.addTurn("Rio", "MineResource",6,6);
            // cy.addTurn("Moscow", "MineResource",7,7);
            // cy.addTurn("Oslo", "MineResource",8,8);
        }     
    })
});

// {"PlayerName":"Professor","action":"BuildIronPlateFactory","coordinates":{"X":0,"Y":0}}

// {"PlayerName":"Professor","action":"BuildSteelFactory","coordinates":{"X":0,"Y":1}}
// {"PlayerName":"Professor","action":"BuildSteelCasingFactory","coordinates":{"X":0,"Y":2}}
// {"PlayerName":"Professor","action":"BuildRocketShellFactory","coordinates":{"X":0,"Y":5}}
// {"PlayerName":"Professor","action":"BuildCopperPlateFactory","coordinates":{"X":0,"Y":6}}
// {"PlayerName":"Professor","action":"BuildWireFactory","coordinates":{"X":0,"Y":8}}
// {"PlayerName":"Professor","action":"BuildCircuitFactory","coordinates":{"X":1,"Y":0}}
// {"PlayerName":"Professor","action":"BuildComputerFactory","coordinates":{"X":1,"Y":1}}
// {"PlayerName":"Professor","action":"BuildPetroleumFactory","coordinates":{"X":1,"Y":2}}
// {"PlayerName":"Professor","action":"BuildBasicFuelFactory","coordinates":{"X":1,"Y":3}}
// {"PlayerName":"Professor","action":"BuildSolidFuelFactory","coordinates":{"X":2,"Y":0}}
// {"PlayerName":"Professor","action":"BuildRocketFuelFactory","coordinates":{"X":2,"Y":2}}
// {"PlayerName":"Professor","action":"BuildMine","coordinates":{"X":5,"Y":17}}