
context('Rocket fuel test complete', () => {
   beforeEach(() => {
      cy.resetGame();
      cy.startGame('Professor');
   });

   it('Rocket!', function () {

      //basic materials
      cy.stoneMining('Professor',6);
      cy.stoneDeposit().should('be.above', 1);

      cy.oilMining('Professor' ,6);
      cy.oilDeposit().should('be.above', 1);


      //factoryes
      cy.buildPetroleumFactoryBuild('Professor',6);
      cy.petroleumDeposit().should('be.above', 1);

      cy.buildBasicFuelFactoryBuild('Professor',6);
      cy.basicFuelDeposit().should('be.above', 1);

      cy.buildSolidFuelFactoryBuild('Professor',6);
      cy.solidFuelDeposit().should('be.above', 1);

      cy.buildRocketFuelFactoryBuild('Professor', 6);
      cy.rocketFuelDeposit().should('be.above', 1);
   })
});

